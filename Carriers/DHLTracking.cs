﻿using System;
using System.Web.Services.Protocols;

namespace TrackShipping
{
    public class DhlTrackingRequest : TrackingRequest
    {
        public string UserId { get; set; }
        public string Password { get; set; }

        public DhlTrackingRequest(string UserId, string Password)
        {
            this.UserId = UserId;
            this.Password = Password;
        }
    }

    public class DhlTracking : TrackingOperation<DhlTrackingRequest>
    {
        TrackShipping.Web_References.DhlTrackingReference.TrackReply metaResponse;
        public override TrackingResponse Track(DhlTrackingRequest Request)
        {
            TrackingResponse response = new TrackingResponse();
            try
            {
                TrackShipping.Web_References.DhlTrackingReference.ECommerce request = CreateTrackRequest(Request);
                TrackShipping.Web_References.DhlTrackingReference.TrackService service = new TrackShipping.Web_References.DhlTrackingReference.TrackService(Request.ServiceUrl);
                try
                {
                    // Call the Track web service passing in a TrackRequest and returning a TrackReply
                    metaResponse = service.Track(request);
                    if (metaResponse.ECommerce == null
                        || metaResponse.ECommerce.Track == null
                        || metaResponse.ECommerce.Track.Shipment == null
                        || metaResponse.ECommerce.Track.Shipment.Result == null
                        || metaResponse.ECommerce.Track.Shipment.Result.Code == "0")
                    {
                        throw new Exception("unexpected tracking info");
                    }

                    response.API_Response = true;
                    response.API_Response_Message = "Success";
                    response.Status = GetStatus();
                    response.ETA = GetEta();
                    response.Destination = GetDestination();
                    response.Activities = metaResponse.Activities;

                }
                catch (SoapException e)
                {
                    response.API_Response = false;
                    response.API_Response_Message = e.Detail.InnerText;
                }
            }
            catch (Exception ex)
            {
                response.API_Response = false;
                response.API_Response_Message = ex.Message;
            }

            return response;
        }

        private static TrackShipping.Web_References.DhlTrackingReference.ECommerce CreateTrackRequest(DhlTrackingRequest Req)
        {
            TrackShipping.Web_References.DhlTrackingReference.ECommerce request = new Web_References.DhlTrackingReference.ECommerce();
            request.Requestor = new Web_References.DhlTrackingReference.Requestor();
            request.Requestor.ID = Req.UserId;
            request.Requestor.Password = Req.Password;

            request.Track = new Web_References.DhlTrackingReference.Track();
            request.Track.Shipment = new Web_References.DhlTrackingReference.Shipment();
            request.Track.Shipment.TrackingNumber = Req.TrackingId;
            return request;
        }

        public override TrackingResponse.STATUS_TYPE GetStatus()
        {
            if (metaResponse.ECommerce != null
                || metaResponse.ECommerce.Track != null
                || metaResponse.ECommerce.Track.Shipment != null
                || metaResponse.ECommerce.Track.Shipment.Result != null
                || metaResponse.ECommerce.Track.Shipment.Result.Code != "0")
            {
                switch (metaResponse.ECommerce.Track.Shipment.Result.Code.ToString())
                {
                    case "CC": 
                    case "DH": 
                    case "GH": 
                    case "HA": 
                    case "IB": 
                    case "OB": 
                    case "PT": 
                    case "DF": 
                    case "TB": 
                    case "AA": 
                    case "AD": 
                    case "AF": 
                    case "EO": 
                    case "FD": 
                    case "IT": 
                    case "LO": 
                    case "DP": 
                    case "DS": 
                    case "PF": 
                    case "PL": 
                    case "TU": 
                    case "PU": 
                    case "SF": 
                    case "AR": 
                    case "CD": 
                    case "AX": 
                    case "OF": 
                    case "PO": 
                    case "DI": 
                        return TrackingResponse.STATUS_TYPE.EN_ROUTE;
                    case "ND":
                    case "NL":
                    case "RF":
                    case "TG":
                        return TrackingResponse.STATUS_TYPE.DELAYED;
                    case "LD":
                    case "PA":
                    case "HL":
                    case "DL":
                        return TrackingResponse.STATUS_TYPE.DELIVERED;
                    case "BA": 
                    case "BD": 
                    case "BN": 
                    case "BT": 
                    case "OD": 
                    case "ED": 
                        return TrackingResponse.STATUS_TYPE.OUT_FOR_DELIVERY;
                    case "AP":
                    case "EP":
                    case "OC":
                    case "OH":
                        return TrackingResponse.STATUS_TYPE.SHIPPING;
                }
            }

            return TrackingResponse.STATUS_TYPE.UNKNOWN;
        }
        public override DateTime? GetEta()
        {
            return null;
        }
        public override string GetWeight()
        {
            if (metaResponse.ECommerce != null
                || metaResponse.ECommerce.Track != null
                || metaResponse.ECommerce.Track.Shipment != null) 
            {
                return metaResponse.ECommerce.Track.Shipment.Weight;
            }
            return "";
        }
        public override string GetPresentTimeStamp()
        {
            return "";
        }
        public override string GetPresentActivity()
        {
            if (metaResponse != null)
            {
                return metaResponse.Activities[0].StateCode.ToString() + " " + metaResponse.Activities[0].TimeStamp.ToString() + " " + metaResponse.Activities[0].Details;
            }

            return "";
        }
        public override TrackingResponse.LocationInfo GetDestination()
        {
            if (metaResponse.ECommerce != null
             || metaResponse.ECommerce.Track != null
             || metaResponse.ECommerce.Track.Shipment != null)
            {
                return metaResponse.ECommerce.Track.Shipment.Location;
            }

            return null;
        }
        public override string GetService()
        {
            return "";
        }

    }
}

