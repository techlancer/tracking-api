﻿using HtmlAgilityPack;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;

namespace TrackShipping
{
    public class AmazonTrackingRequest : TrackingRequest
    {
        public string OrderId { get; set; }

        public string OrderingShipmentId { get; set; }

        public AmazonTrackingRequest(string OrderId, string ShipmentId)
        {
            CarrierName = "Amazon";
            this.OrderId = OrderId;
            this.OrderingShipmentId = ShipmentId;
        }
    }

    public class AmazonTracking : TrackingOperation<AmazonTrackingRequest>
    {
        private TrackingResponse response = new TrackingResponse();

        public override TrackingResponse Track(AmazonTrackingRequest Request)
        {
            try
            {
                string queryString = "ref=pe_385040_121528360_TE_typ?orderID=" + Request.OrderId + "&orderingShipmentId=" + Request.OrderingShipmentId + "&packageId=1";
                HttpWebRequest httpWReq = (HttpWebRequest)WebRequest.Create(Request.ServiceUrl + queryString);
                httpWReq.Method = "GET";
                HttpWebResponse httpResponse = (HttpWebResponse)httpWReq.GetResponse();
                string responseString = new StreamReader(httpResponse.GetResponseStream()).ReadToEnd();
                responseString = WebUtility.HtmlDecode(responseString);
                HtmlDocument resultat = new HtmlDocument();
                resultat.LoadHtml(responseString);

                response.API_Response = true;
                response.API_Response_Message = "Success";

                //
                // Capturing status from tracking response
                //
                List<HtmlNode> h2Elements = resultat.DocumentNode.Descendants().Where
                       (x => (x.Name == "h2")).ToList();

                if (h2Elements != null && h2Elements.ToList().Count > 0)
                {
                    string statusText = h2Elements[0].InnerText;
                    response.Status = MapStatus(statusText);
                }

                //
                // Capturing EAT from tracking response
                //
                List<HtmlNode> deliveryElements = resultat.DocumentNode.Descendants().Where
                      (x => (x.Name == "span")).ToList();

                if (deliveryElements != null)
                {
                    foreach (var ele in deliveryElements)
                    {
                        if (ele.InnerText.Equals("Expected delivery"))
                        {
                            string etaString = ele.NextSibling.InnerText;
                            DateTime eta;
                            if (DateTime.TryParse(etaString, out eta))
                            {
                                response.ETA = eta.Date;
                            }
                        }
                    }
                }

                //
                // Capturing Activites and Status from tracking response
                //
                if (response.Status == TrackingResponse.STATUS_TYPE.DELIVERED)
                {
                    string tsString = string.Empty;
                    List<HtmlNode> avtivityElements = resultat.DocumentNode.Descendants().Where
                            (x => (x.Name == "span")).ToList();

                    List<TrackingResponse.ActivityInfo> activities = new List<TrackingResponse.ActivityInfo>();
                    if (avtivityElements != null)
                    {
                        foreach (var ele in avtivityElements)
                        {
                            TrackingResponse.ActivityInfo activity = new TrackingResponse.ActivityInfo();
                            if (ele.InnerText.Equals("Delivered on:"))
                            {
                                tsString = ele.NextSibling.InnerText;
                                DateTime eta;
                                if (DateTime.TryParse(tsString, out eta))
                                {
                                    activity.TimeStamp = eta.ToString();
                                    activity.Details = "Delivered";
                                    activities.Add(activity);
                                }
                            }
                        }
                    }

                    response.Activities = activities;
                }

            }
            catch (Exception ex)
            {
                response.API_Response = false;
                response.API_Response_Message = ex.Message;
            }

            return response;
        }

        public override DateTime? GetEta()
        {
            if (response != null)
                return response.ETA;
            return null;
        }
        public override string GetWeight()
        {
            return "";
        }
        public override string GetPresentTimeStamp()
        {
            return "";
        }
        public override string GetPresentActivity()
        {
            if ( response!= null && response.Activities != null && response.Activities.Count > 0)
            {
                var trackDetail = response.Activities[0];
                return trackDetail.TimeStamp + " " + trackDetail.Details;
            }
            return "";
        }
        public override TrackingResponse.LocationInfo GetDestination()
        {
            return null;
        }
        public override string GetService()
        {
            return "";
        }

        public override TrackingResponse.STATUS_TYPE GetStatus()
        {
            if (response != null)
            {
                return response.Status;
            }

            return TrackingResponse.STATUS_TYPE.UNKNOWN;
        }

        private TrackingResponse.STATUS_TYPE MapStatus(string status)
        {
            switch (status)
            {
                case "shipping soon":
                    return TrackingResponse.STATUS_TYPE.SHIPPING;
                case "delivered":
                    return TrackingResponse.STATUS_TYPE.DELIVERED;
                case "out for delivery":
                    return TrackingResponse.STATUS_TYPE.OUT_FOR_DELIVERY;
                case "transit":
                    return TrackingResponse.STATUS_TYPE.EN_ROUTE;
                default:
                    return TrackingResponse.STATUS_TYPE.UNKNOWN;
            }
        }
    }
}
