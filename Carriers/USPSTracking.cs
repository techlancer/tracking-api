﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Text;
using System.Xml.Serialization;

namespace TrackShipping
{
    public class USPSTrackingRequest : TrackingRequest
    {
        public string UserId { get; set; }

        public USPSTrackingRequest(string UserId, string TrackingId)
        {
            CarrierName = "USPS";
            this.UserId = UserId;
            this.TrackingId = TrackingId;
        }
    }

    public class USPSTracking : TrackingOperation<USPSTrackingRequest>
    {
        USPSTrackingMetaResponse.TrackResponse uspsTrackingMetaResponse;
        public override TrackingResponse Track(USPSTrackingRequest Request)
        {
            TrackingResponse response = new TrackingResponse();
            
            try
            {
                HttpWebRequest httpWReq = (HttpWebRequest)WebRequest.Create(Request.ServiceUrl);
                ASCIIEncoding encoding = new ASCIIEncoding();

                string postData = "API=TrackV2&XML=<?xml version=\"1.0\" encoding=\"UTF-8\" ?><TrackFieldRequest USERID=\"" + Request.UserId + "\"><Revision>1</Revision><ClientIp>127.0.0.1</ClientIp><SourceId>TrackShipping</SourceId><TrackID ID=\"" + Request.TrackingId + "\"></TrackID></TrackFieldRequest>";
                byte[] data = encoding.GetBytes(postData);

                httpWReq.Method = "POST";
                httpWReq.ContentType = "application/x-www-form-urlencoded";
                httpWReq.ContentLength = data.Length;

                using (Stream stream = httpWReq.GetRequestStream())
                {
                    stream.Write(data, 0, data.Length);
                }

                System.Net.ServicePointManager.Expect100Continue = false;
                HttpWebResponse httpResponse = (HttpWebResponse)httpWReq.GetResponse();
                string responseString = new StreamReader(httpResponse.GetResponseStream()).ReadToEnd();
                if (responseString.ToLower().Contains("error"))
                {

                    var serializer = new XmlSerializer(typeof(USPSTrackingMetaResponse.Error));
                    USPSTrackingMetaResponse.Error result;

                    using (TextReader reader = new StringReader(responseString))
                    {
                        result = (USPSTrackingMetaResponse.Error)serializer.Deserialize(reader);
                    }

                    response.API_Response = false;
                    response.API_Response_Message = result.Description.ToString();
                }

                else
                {

                    var serializer = new XmlSerializer(typeof(USPSTrackingMetaResponse.TrackResponse));
                    
                    using (TextReader reader = new StringReader(responseString))
                    {
                        uspsTrackingMetaResponse = (USPSTrackingMetaResponse.TrackResponse)serializer.Deserialize(reader);
                    }
                    

                    response.API_Response = true;
                    response.API_Response_Message = "Success";
                    response.Summary = uspsTrackingMetaResponse.TrackInfo.TrackSummary.Event;

                    response.Status = GetStatus();
                    response.ETA = GetEta();
                    response.Destination = GetDestination();
                }
            }
            catch (Exception ex)
            {
                response.API_Response = false;
                response.API_Response_Message = ex.Message;
            }

            return response;
        }
        public override TrackingResponse.STATUS_TYPE GetStatus()
        {
            switch(uspsTrackingMetaResponse.TrackInfo.StatusCategory)
            {
                case "Pre-Shipment":
                    return TrackingResponse.STATUS_TYPE.SHIPPING;
                case "Delivered":
                    return TrackingResponse.STATUS_TYPE.DELIVERED;
                default:
                    {
                        switch(uspsTrackingMetaResponse.TrackInfo.Status)
                        {
                            case "Accept":
                            case "Processed":
                            case "Depart":
                            case "Picked Up":
                            case "Arrival":
                            case "Sorting Complete":
                            case "Customs clearance":
                            case "Dispatch":
                            case "Arrive":
                            case "Inbound Out of Customs":
                            case "Forwarded":
                                return TrackingResponse.STATUS_TYPE.EN_ROUTE;
                            case "Out for Delivery":
                                return TrackingResponse.STATUS_TYPE.OUT_FOR_DELIVERY;
                            case "Delivered":
                                return TrackingResponse.STATUS_TYPE.DELIVERED;
                            case "Notice Left":
                                return TrackingResponse.STATUS_TYPE.DELAYED;
                            case "Refused":
                                return TrackingResponse.STATUS_TYPE.DELAYED;
                            case "Item being held":
                                return TrackingResponse.STATUS_TYPE.DELAYED;
                            case "Missed delivery":
                                return TrackingResponse.STATUS_TYPE.DELAYED;
                            case "Addressee not available":
                                return TrackingResponse.STATUS_TYPE.DELAYED;
                            case "Undeliverable as Addressed":
                                return TrackingResponse.STATUS_TYPE.DELAYED;
                        }
                        break;
                    }
            }
            return TrackingResponse.STATUS_TYPE.UNKNOWN;
        }
        public override DateTime? GetEta()
        {
            string predictedDeliveryDate = uspsTrackingMetaResponse.TrackInfo.PredictedDeliveryDate;
            DateTime temp = DateTime.Now;
            if (DateTime.TryParse(predictedDeliveryDate, out temp))
                return temp;
            return null;
        }
        public override string GetWeight()
        {            
            return "";
        }
        public override string GetPresentTimeStamp()
        {
            if(uspsTrackingMetaResponse.TrackInfo.TrackDetail.Count > 0)
            {
                return uspsTrackingMetaResponse.TrackInfo.TrackDetail[0].EventDate + " " + uspsTrackingMetaResponse.TrackInfo.TrackDetail[0].EventTime;
            }
            return "";
        }
        public override string GetPresentActivity()
        {
            if(uspsTrackingMetaResponse.TrackInfo.TrackDetail.Count > 0)
            {
                USPSTrackingMetaResponse.TrackDetail trackDetail = uspsTrackingMetaResponse.TrackInfo.TrackDetail[0];
                string location = trackDetail.EventCity + "," + trackDetail.EventState + "," + trackDetail.EventCountry + "," + trackDetail.EventZIPCode;
                return GetPresentTimeStamp() + " " + location;
            }
            return "";
        }
        public override TrackingResponse.LocationInfo GetDestination()
        {
            TrackingResponse.LocationInfo locationInfo = new TrackingResponse.LocationInfo()
            {
                City = uspsTrackingMetaResponse.TrackInfo.DestinationCity,
                State = uspsTrackingMetaResponse.TrackInfo.DestinationState,
                PostalCode = uspsTrackingMetaResponse.TrackInfo.DestinationZip
            };
            return locationInfo;
        }
        public override string GetService()
        {
            return "";
        }
    }

    public class USPSTrackingMetaResponse
    {
        public partial class Error
        {

            private string numberField;

            private string descriptionField;

            private string sourceField;

            /// <remarks/>
            public string Number
            {
                get
                {
                    return this.numberField;
                }
                set
                {
                    this.numberField = value;
                }
            }

            /// <remarks/>
            public string Description
            {
                get
                {
                    return this.descriptionField;
                }
                set
                {
                    this.descriptionField = value;
                }
            }

            /// <remarks/>
            public string Source
            {
                get
                {
                    return this.sourceField;
                }
                set
                {
                    this.sourceField = value;
                }
            }
        }

        [XmlRoot(ElementName = "TrackSummary")]
        public class TrackSummary
        {
            [XmlElement(ElementName = "EventTime")]
            public string EventTime { get; set; }
            [XmlElement(ElementName = "EventDate")]
            public string EventDate { get; set; }
            [XmlElement(ElementName = "Event")]
            public string Event { get; set; }
            [XmlElement(ElementName = "EventCity")]
            public string EventCity { get; set; }
            [XmlElement(ElementName = "EventState")]
            public string EventState { get; set; }
            [XmlElement(ElementName = "EventZIPCode")]
            public string EventZIPCode { get; set; }
            [XmlElement(ElementName = "EventCountry")]
            public string EventCountry { get; set; }
            [XmlElement(ElementName = "FirmName")]
            public string FirmName { get; set; }
            [XmlElement(ElementName = "Name")]
            public string Name { get; set; }
            [XmlElement(ElementName = "AuthorizedAgent")]
            public string AuthorizedAgent { get; set; }
            [XmlElement(ElementName = "EventCode")]
            public string EventCode { get; set; }
            [XmlElement(ElementName = "DeliveryAttributeCode")]
            public string DeliveryAttributeCode { get; set; }
        }

        [XmlRoot(ElementName = "TrackDetail")]
        public class TrackDetail
        {
            [XmlElement(ElementName = "EventTime")]
            public string EventTime { get; set; }
            [XmlElement(ElementName = "EventDate")]
            public string EventDate { get; set; }
            [XmlElement(ElementName = "Event")]
            public string Event { get; set; }
            [XmlElement(ElementName = "EventCity")]
            public string EventCity { get; set; }
            [XmlElement(ElementName = "EventState")]
            public string EventState { get; set; }
            [XmlElement(ElementName = "EventZIPCode")]
            public string EventZIPCode { get; set; }
            [XmlElement(ElementName = "EventCountry")]
            public string EventCountry { get; set; }
            [XmlElement(ElementName = "FirmName")]
            public string FirmName { get; set; }
            [XmlElement(ElementName = "Name")]
            public string Name { get; set; }
            [XmlElement(ElementName = "AuthorizedAgent")]
            public string AuthorizedAgent { get; set; }
            [XmlElement(ElementName = "EventCode")]
            public string EventCode { get; set; }
        }

        [XmlRoot(ElementName = "TrackInfo")]
        public class TrackInfo
        {
            [XmlElement(ElementName = "Class")]
            public string Class { get; set; }
            [XmlElement(ElementName = "ClassOfMailCode")]
            public string ClassOfMailCode { get; set; }
            [XmlElement(ElementName = "DestinationCity")]
            public string DestinationCity { get; set; }
            [XmlElement(ElementName = "DestinationState")]
            public string DestinationState { get; set; }
            [XmlElement(ElementName = "DestinationZip")]
            public string DestinationZip { get; set; }
            [XmlElement(ElementName = "EmailEnabled")]
            public string EmailEnabled { get; set; }
            [XmlElement(ElementName = "KahalaIndicator")]
            public string KahalaIndicator { get; set; }
            [XmlElement(ElementName = "MailTypeCode")]
            public string MailTypeCode { get; set; }
            [XmlElement(ElementName = "MPDATE")]
            public string MPDATE { get; set; }
            [XmlElement(ElementName = "MPSUFFIX")]
            public string MPSUFFIX { get; set; }
            [XmlElement(ElementName = "OriginCity")]
            public string OriginCity { get; set; }
            [XmlElement(ElementName = "OriginState")]
            public string OriginState { get; set; }
            [XmlElement(ElementName = "OriginZip")]
            public string OriginZip { get; set; }
            [XmlElement(ElementName = "PodEnabled")]
            public string PodEnabled { get; set; }
            [XmlElement(ElementName = "PredictedDeliveryDate")]
            public string PredictedDeliveryDate { get; set; }
            [XmlElement(ElementName = "RestoreEnabled")]
            public string RestoreEnabled { get; set; }
            [XmlElement(ElementName = "RramEnabled")]
            public string RramEnabled { get; set; }
            [XmlElement(ElementName = "RreEnabled")]
            public string RreEnabled { get; set; }
            [XmlElement(ElementName = "Service")]
            public string Service { get; set; }
            [XmlElement(ElementName = "ServiceTypeCode")]
            public string ServiceTypeCode { get; set; }
            [XmlElement(ElementName = "Status")]
            public string Status { get; set; }
            [XmlElement(ElementName = "StatusCategory")]
            public string StatusCategory { get; set; }
            [XmlElement(ElementName = "StatusSummary")]
            public string StatusSummary { get; set; }
            [XmlElement(ElementName = "TABLECODE")]
            public string TABLECODE { get; set; }
            [XmlElement(ElementName = "TrackSummary")]
            public TrackSummary TrackSummary { get; set; }
            [XmlElement(ElementName = "TrackDetail")]
            public List<TrackDetail> TrackDetail { get; set; }
            [XmlAttribute(AttributeName = "ID")]
            public string ID { get; set; }
        }

        [XmlRoot(ElementName = "TrackResponse")]
        public class TrackResponse
        {
            [XmlElement(ElementName = "TrackInfo")]
            public TrackInfo TrackInfo { get; set; }
        }
    }
}
