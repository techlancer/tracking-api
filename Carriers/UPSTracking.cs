﻿using System;
using System.Collections.Generic;
using System.Web;
using TrackShipping.TrackWebReference;

namespace TrackShipping
{
    public class UPSTrackingRequest : TrackingRequest
    {
        public string AccessLicenseNumber { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }

        public UPSTrackingRequest(string AccessLicenseNumber,string Username, string Password,string TrackingId)
        {
            this.AccessLicenseNumber = AccessLicenseNumber;
            this.Username = Username;
            this.Password = Password;
            this.TrackingId = TrackingId;
            CarrierName = "UPS";
        }
    }
    public class UPSTracking : TrackingOperation<UPSTrackingRequest>
    {
        TrackResponse metaResponse;
        public override TrackingResponse Track(UPSTrackingRequest request)
        {
            TrackingResponse response = new TrackingResponse();
            List<string> PackageHistory = new List<string>();

            try
            {
                TrackService track = new TrackService(request.ServiceUrl);
                TrackRequest tr = new TrackRequest();
                UPSSecurity upss = new UPSSecurity();
                UPSSecurityServiceAccessToken upssSvcAccessToken = new UPSSecurityServiceAccessToken();
                upssSvcAccessToken.AccessLicenseNumber = request.AccessLicenseNumber;
                upss.ServiceAccessToken = upssSvcAccessToken;
                upss.UsernameToken = new UPSSecurityUsernameToken()
                {
                    Username = request.Username,
                    Password = request.Password
                };
                track.UPSSecurityValue = upss;
                RequestType requestType = new RequestType();
                String[] requestOption = { "15" };
                requestType.RequestOption = requestOption;
                tr.Request = requestType;
                tr.InquiryNumber = request.TrackingId;
                System.Net.ServicePointManager.CertificatePolicy = new TrustAllCertificatePolicy();
                
                metaResponse = track.ProcessTrack(tr);

                response.API_Response = true;
                response.API_Response_Message = "Success";

                response.Status = GetStatus();

                foreach (var item in metaResponse.Shipment[0].Package[0].Activity)
                {
                    string city = string.IsNullOrEmpty(item.ActivityLocation.Address.City) == true ? "" : item.ActivityLocation.Address.City;
                    string countrycode = string.IsNullOrEmpty(item.ActivityLocation.Address.CountryCode) == true ? "" : item.ActivityLocation.Address.CountryCode;
                    string statecode = string.IsNullOrEmpty(item.ActivityLocation.Address.StateProvinceCode) == true ? "" : item.ActivityLocation.Address.StateProvinceCode;
                    string postalcode = string.IsNullOrEmpty(item.ActivityLocation.Address.PostalCode) == true ? "" : item.ActivityLocation.Address.PostalCode;
                    string date = string.IsNullOrEmpty(item.Date) == true ? "" : item.Date;
                    string time = string.IsNullOrEmpty(item.Time) == true ? "" : item.Time;
                    string status = string.IsNullOrEmpty(item.Status.Description) == true ? "" : item.Status.Description;

                    response.Activities.Add(new TrackingResponse.ActivityInfo() { 
                        City = city, 
                        CountryCode = countrycode, 
                        TimeStamp = date, 
                        PostalCode = postalcode, 
                        StateCode = statecode, 
                        Details = status 
                    });                    
                }
                response.Summary = metaResponse.Shipment[0].Package[0].Activity[0].Status.Description;
                response.Status = GetStatus();
                response.ETA = GetEta();
                response.Destination = GetDestination();
            }
            catch (System.Web.Services.Protocols.SoapException ex)
            {
                response.API_Response = false;
                response.API_Response_Message = "SoapException Message= " + ex.Message + "  " + "SoapException Category:Code:Message= " + ex.Detail.LastChild.InnerText + "  " + "SoapException XML String for all= " + ex.Detail.LastChild.OuterXml;
            }
            catch (System.ServiceModel.CommunicationException ex)
            {
                response.API_Response = false;
                response.API_Response_Message = ex.Message;
            }
            catch (Exception ex)
            {
                response.API_Response = false;
                response.API_Response_Message = ex.Message;
            }

            return response;
        }

        public override TrackingResponse.STATUS_TYPE GetStatus()
        {
            StatusType Status = metaResponse.Shipment[0].Package[0].Activity[0].Status;
            TrackShipping.TrackingResponse.STATUS_TYPE retStatus = TrackingResponse.STATUS_TYPE.UNKNOWN;
            switch(Status.Type)
            {
                case "D":
                    retStatus = TrackingResponse.STATUS_TYPE.DELIVERED;
                    break;
                case "P":
                    retStatus = TrackingResponse.STATUS_TYPE.EN_ROUTE;
                    break;
                case "M":
                    retStatus = TrackingResponse.STATUS_TYPE.SHIPPING;
                    break;
                case "I":
                    {
                        if(Status.Code == "OF")
                            retStatus = TrackingResponse.STATUS_TYPE.OUT_FOR_DELIVERY;
                        else
                            retStatus = TrackingResponse.STATUS_TYPE.EN_ROUTE;
                    }
                    break;
                case "X":
                    {
                        if (Status.Code == "U2")
                            retStatus = TrackingResponse.STATUS_TYPE.EN_ROUTE;
                        else
                            retStatus = TrackingResponse.STATUS_TYPE.DELAYED;
                    }
                    break;
                default:
                    retStatus = TrackShipping.TrackingResponse.STATUS_TYPE.UNKNOWN;
                    break;
            }
            return retStatus;
        }
        public override DateTime? GetEta()
        {
            if(metaResponse.Shipment != null 
                && metaResponse.Shipment.Length > 0 
                && metaResponse.Shipment[0].DeliveryDetail != null 
                && metaResponse.Shipment[0].DeliveryDetail.Length > 0)
            {
                string estimatedDateStr = metaResponse.Shipment[0].DeliveryDetail[0].Date;
                DateTime temp = DateTime.Now;
                if(DateTime.TryParse(estimatedDateStr,out temp))
                {
                    return temp;
                }
            }
            
            return null;
        }
        public override string GetWeight()
        {
            return "";
        }
        public override string GetPresentTimeStamp()
        {
            if(metaResponse.Shipment != null 
                && metaResponse.Shipment.Length > 0 
                && metaResponse.Shipment[0].Activity != null
                && metaResponse.Shipment[0].Activity.Length > 0)
            {
                return metaResponse.Shipment[0].Activity[0].Date;
            }
            return "";
        }
        public override string GetPresentActivity()
        {
            if (metaResponse.Shipment != null
                && metaResponse.Shipment.Length > 0
                && metaResponse.Shipment[0].Activity != null
                && metaResponse.Shipment[0].Activity.Length > 0)
            {
                return metaResponse.Shipment[0].Activity[0].ActivityLocation.City + metaResponse.Shipment[0].Activity[0].ActivityLocation.StateProvinceCode;
            }
            return "";

        }
        public override TrackingResponse.LocationInfo GetDestination()
        {
            TrackingResponse.LocationInfo locationInfo = new TrackingResponse.LocationInfo();
            if (metaResponse.Shipment != null
                && metaResponse.Shipment.Length > 0
                && metaResponse.Shipment[0].ShipmentAddress != null
                && metaResponse.Shipment[0].ShipmentAddress.Length > 0
                && metaResponse.Shipment[0].ShipmentAddress[0].Address != null)
            {
                locationInfo.City = metaResponse.Shipment[0].ShipmentAddress[0].Address.City;
                locationInfo.State = metaResponse.Shipment[0].ShipmentAddress[0].Address.StateProvinceCode;
                locationInfo.PostalCode = metaResponse.Shipment[0].ShipmentAddress[0].Address.PostalCode;
            }

            return locationInfo;
        }
        public override string GetService()
        {
            return "";
        }
    }
}
