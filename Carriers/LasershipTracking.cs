﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace TrackShipping
{
    public class LasershipTracking : TrackingOperation<TrackingRequest>
    {
        LaserShipTrackingMetaResponse laserShipTrackingMetaResponse = null;
        public override TrackingResponse Track(TrackingRequest request)
        {
            TrackingResponse response = new TrackingResponse();

            try
            {
                string URL = "http://www.lasership.com/track/" + request.TrackingId + "/json";
                HttpWebRequest httpWReq = (HttpWebRequest)WebRequest.Create(URL);
                ASCIIEncoding encoding = new ASCIIEncoding();

                httpWReq.Method = "GET";
                httpWReq.ContentType = "application/x-www-form-urlencoded";

                System.Net.ServicePointManager.Expect100Continue = false;
                HttpWebResponse httpResponse = (HttpWebResponse)httpWReq.GetResponse();
                string res = new StreamReader(httpResponse.GetResponseStream()).ReadToEnd();

                laserShipTrackingMetaResponse = (LaserShipTrackingMetaResponse)Utils.Deserialize(res, typeof(LaserShipTrackingMetaResponse));

                foreach (var item in laserShipTrackingMetaResponse.Events)
                {
                    string city = string.IsNullOrEmpty(item.City) == true ? "" : item.City;
                    string country = string.IsNullOrEmpty(item.Country) == true ? "" : item.Country;
                    string postalcode = string.IsNullOrEmpty(item.PostalCode) == true ? "" : item.PostalCode;
                    string state = string.IsNullOrEmpty(item.State) == true ? "" : item.State;
                    string datetime = string.IsNullOrEmpty(item.DateTime) == true ? "" : item.DateTime;
                    string eventshorttext = string.IsNullOrEmpty(item.EventShortText) == true ? "" : item.EventShortText;

                    response.Activities.Add(new TrackingResponse.ActivityInfo()
                    {
                         City = city, Details = eventshorttext, CountryCode = country, PostalCode = postalcode, StateCode = state, TimeStamp = datetime
                    });
                }

                response.API_Response = true;
                response.API_Response_Message = "Success";
                response.Summary = laserShipTrackingMetaResponse.Events[0].EventShortText;
            }
            catch (Exception ex)
            {
                response.API_Response = false;
                response.API_Response_Message = ex.Message;
            }

            return response;
        }
        public override TrackingResponse.STATUS_TYPE GetStatus()
        {
            return TrackingResponse.STATUS_TYPE.UNKNOWN;
        }
        public override DateTime? GetEta()
        {
            return DateTime.Now;
        }
        public override string GetWeight()
        {
            return "";
        }
        public override string GetPresentTimeStamp()
        {
            return "";
        }
        public override string GetPresentActivity()
        {
            return "";
        }
        public override TrackingResponse.LocationInfo GetDestination()
        {
            return null;
        }
        public override string GetService()
        {
            return "";
        }
    }

    public class LaserShipTrackingMetaResponse
    {
        public class Origin
        {
            public string City { get; set; }
            public string State { get; set; }
            public string PostalCode { get; set; }
            public string Country { get; set; }
        }

        public class Destination
        {
            public string City { get; set; }
            public string State { get; set; }
            public string PostalCode { get; set; }
            public string Country { get; set; }
        }

        public class Piece
        {
            public string TrackingNumber { get; set; }
            public string EventType { get; set; }
            public string EventShortText { get; set; }
            public int Weight { get; set; }
            public string WeightUnit { get; set; }
        }

        public class Event
        {
            public string DateTime { get; set; }
            public string UTCDateTime { get; set; }
            public string City { get; set; }
            public string State { get; set; }
            public string PostalCode { get; set; }
            public string Country { get; set; }
            public string EventType { get; set; }
            public string EventShortText { get; set; }
            public string Signature { get; set; }
            public string Signature2 { get; set; }
            public string Location { get; set; }
            public string Reason { get; set; }
        }

        public string OrderNumber { get; set; }
        public string ReceivedOn { get; set; }
        public string UTCReceivedOn { get; set; }
        public string EstimatedDeliveryDate { get; set; }
        public Origin origin { get; set; }
        public Destination destination { get; set; }
        public List<Piece> Pieces { get; set; }
        public List<Event> Events { get; set; }
    }
}
