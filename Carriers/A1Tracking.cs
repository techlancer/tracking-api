﻿using System;
using System.Text.RegularExpressions;
using System.Web.Services.Protocols;

namespace TrackShipping
{
    public class A1TrackingRequest : TrackingRequest
    { }

    public class A1Tracking : TrackingOperation<A1TrackingRequest>
    {
        TrackShipping.Web_References.A1TrackingReference.TrackReply metaResponse;
        public override TrackingResponse Track(A1TrackingRequest Request)
        {
            TrackingResponse response = new TrackingResponse();
            try
            {
                TrackingRequest request = new TrackingRequest();
                request.ServiceUrl = Request.ServiceUrl;
                TrackShipping.Web_References.A1TrackingReference.TrackService service = new TrackShipping.Web_References.A1TrackingReference.TrackService(Request.ServiceUrl);

                try
                {
                    // Call the Track web service passing in a TrackRequest and returning a TrackReply
                    metaResponse = service.Track(request);
                    if (metaResponse.AmazonTrackingResponse == null
                        || metaResponse.AmazonTrackingResponse.TrackingErrorInfo != null
                        || metaResponse.AmazonTrackingResponse.TrackingErrorInfo.TrackingErrorDetail.Count > 0)
                        
                    {
                        throw new Exception(metaResponse.AmazonTrackingResponse.TrackingErrorInfo.TrackingErrorDetail[0].ErrorDetailCodeDesc);
                    }

                    response.API_Response = true;
                    response.API_Response_Message = "Success";
                    response.Status = GetStatus();
                    response.ETA = GetEta();
                    response.Destination = GetDestination();
                    response.Activities = metaResponse.Activities;

                }
                catch (SoapException e)
                {
                    response.API_Response = false;
                    response.API_Response_Message = e.Detail.InnerText;
                }
            }
            catch (Exception ex)
            {
                response.API_Response = false;
                response.API_Response_Message = ex.Message;
            }

            return response;
        }
        public override TrackingResponse.STATUS_TYPE GetStatus()
        {
            TrackingResponse.STATUS_TYPE status = TrackingResponse.STATUS_TYPE.UNKNOWN;

            if (metaResponse.AmazonTrackingResponse != null
                || metaResponse.AmazonTrackingResponse.TrackingEventHistory != null
                || metaResponse.AmazonTrackingResponse.TrackingEventHistory.Count > 0
                || metaResponse.AmazonTrackingResponse.TrackingEventHistory[0].TrackingEventDetail != null
                || metaResponse.AmazonTrackingResponse.TrackingEventHistory[0].TrackingEventDetail.Count > 0)
            {
                string statusCode = metaResponse.AmazonTrackingResponse.TrackingEventHistory[0].TrackingEventDetail[0].EventCode;
                Match match = Regex.Match(statusCode, "/EVENT_(.*)$/");
                if (match.Success)
                {
                    statusCode = match.Captures[1].Value;
                }

                switch (statusCode)
                {
                    case "101": 
                    case "102": 
                        status = TrackingResponse.STATUS_TYPE.EN_ROUTE;
                        break;
                    case "302": 
                        status = TrackingResponse.STATUS_TYPE.OUT_FOR_DELIVERY;
                        break;
                    case "304": 
                        status = TrackingResponse.STATUS_TYPE.DELAYED;
                        break;
                    case "301":
                        status = TrackingResponse.STATUS_TYPE.DELIVERED;
                        break;
                }
            }

            return status;
        }
        public override DateTime? GetEta()
        {
            if (metaResponse.AmazonTrackingResponse != null
            || metaResponse.AmazonTrackingResponse.TrackingEventHistory != null
            || metaResponse.AmazonTrackingResponse.TrackingEventHistory.Count > 0
            || metaResponse.AmazonTrackingResponse.TrackingEventHistory[0].TrackingEventDetail != null
            || metaResponse.AmazonTrackingResponse.TrackingEventHistory[0].TrackingEventDetail.Count > 0)
            {
                return metaResponse.AmazonTrackingResponse.TrackingEventHistory[0].TrackingEventDetail[0].EstimatedDeliveryDate.Date;
            }

            return DateTime.MinValue;
        }
        public override string GetWeight()
        {
            return "";
        }
        public override string GetPresentTimeStamp()
        {
            return "";
        }
        public override string GetPresentActivity()
        {
            if (metaResponse != null)
            {
                return metaResponse.Activities[0].StateCode.ToString() + " " + metaResponse.Activities[0].TimeStamp.ToString() + " " + metaResponse.Activities[0].Details;
            }

            return "";
        }
        public override TrackingResponse.LocationInfo GetDestination()
        {
            TrackingResponse.LocationInfo locationInfo = new TrackingResponse.LocationInfo();
            if (metaResponse.AmazonTrackingResponse != null
            || metaResponse.AmazonTrackingResponse.TrackingEventHistory != null
            || metaResponse.AmazonTrackingResponse.TrackingEventHistory.Count > 0
            || metaResponse.AmazonTrackingResponse.TrackingEventHistory[0].TrackingEventDetail != null
            || metaResponse.AmazonTrackingResponse.TrackingEventHistory[0].TrackingEventDetail.Count > 0)
            {
                if(metaResponse.AmazonTrackingResponse.TrackingEventHistory[0].TrackingEventDetail[0].PackageDestinationLocation != null)
                {
                    return metaResponse.AmazonTrackingResponse.TrackingEventHistory[0].TrackingEventDetail[0].PackageDestinationLocation;
                }
            }

            return null;
        }
        public override string GetService()
        {
            return "";
        }
    }
}

