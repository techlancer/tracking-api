﻿using System;
using System.Collections.Generic;
using System.Web.Services.Protocols;
using TrackShipping.TrackServiceWebReference;

namespace TrackShipping
{
    public class FedexTrackingRequest : TrackingRequest
    {
        public string Key { get; set; }
        public string Password { get; set; }
        public string AccountNumber { get; set; }
        public string MeterNumber { get; set; }

        public FedexTrackingRequest(string Key, string Password, string AccountNumber, string MeterNumber,string TrackingNumber)
        {
            this.Key = Key;
            this.Password = Password;
            this.AccountNumber = AccountNumber;
            this.MeterNumber = MeterNumber;
            this.TrackingId = TrackingNumber;
        }
    }

    public class FedexTracking : TrackingOperation<FedexTrackingRequest>
    {
        TrackReply metaResponse;
        public override TrackingResponse Track(FedexTrackingRequest Request)
        {
            TrackingResponse response = new TrackingResponse();
            try
            {
                TrackRequest request = CreateTrackRequest(Request);
                TrackService service = new TrackService(Request.ServiceUrl);
                try
                {
                    // Call the Track web service passing in a TrackRequest and returning a TrackReply
                    metaResponse = service.track(request);
                    if (metaResponse.HighestSeverity == NotificationSeverityType.SUCCESS 
                            || metaResponse.HighestSeverity == NotificationSeverityType.NOTE
                            || metaResponse.HighestSeverity == NotificationSeverityType.WARNING)
                    {
                        foreach (var item in metaResponse.CompletedTrackDetails[0].TrackDetails[0].Events)
                        {
                            string city = string.IsNullOrEmpty(item.Address.City) == true ? "" : item.Address.City;
                            string countrycode = string.IsNullOrEmpty(item.Address.CountryCode) == true ? "" : item.Address.CountryCode;
                            string datetime = string.IsNullOrEmpty(item.Timestamp.ToLongDateString()) == true ? "" : item.Timestamp.ToLongDateString();
                            string status = string.IsNullOrEmpty(item.EventDescription) == true ? "" : item.EventDescription;
                            string postalcode = string.IsNullOrEmpty(item.Address.PostalCode) == true ? "" : item.Address.PostalCode;
                            string statecode = string.IsNullOrEmpty(item.Address.StateOrProvinceCode) == true ? "" : item.Address.StateOrProvinceCode;

                            response.Activities.Add(new TrackingResponse.ActivityInfo()
                            {
                                City = city,
                                CountryCode = countrycode, 
                                TimeStamp = datetime, 
                                Details = status
                            });
                        }

                        response.API_Response = true;
                        response.API_Response_Message = "Success";
                        response.Summary = metaResponse.CompletedTrackDetails[0].TrackDetails[0].Events[0].EventDescription;
                        response.Status = GetStatus();
                        response.ETA = GetEta();
                        response.Destination = GetDestination();
                    }

                }
                catch (SoapException e)
                {
                    response.API_Response = false;
                    response.API_Response_Message = e.Detail.InnerText;
                }
            }
            catch (Exception ex)
            {
                response.API_Response = false;
                response.API_Response_Message = ex.Message;
            }

            return response;
        }

        private static TrackRequest CreateTrackRequest(FedexTrackingRequest Req)
        {
            // Build the TrackRequest
            TrackRequest request = new TrackRequest();
            //
            request.WebAuthenticationDetail = new WebAuthenticationDetail();
            request.WebAuthenticationDetail.UserCredential = new WebAuthenticationCredential();
            request.WebAuthenticationDetail.UserCredential.Key = Req.Key; 
            request.WebAuthenticationDetail.UserCredential.Password = Req.Password; 
           

            request.ClientDetail = new ClientDetail();
            request.ClientDetail.AccountNumber = Req.AccountNumber;
            request.ClientDetail.MeterNumber = Req.MeterNumber;
            
            request.TransactionDetail = new TransactionDetail();
            request.TransactionDetail.CustomerTransactionId = Req.TrackingId;

            request.Version = new VersionId();
            //
            // Tracking information
            request.SelectionDetails = new TrackSelectionDetail[1] { new TrackSelectionDetail() };
            request.SelectionDetails[0].PackageIdentifier = new TrackPackageIdentifier();
            request.SelectionDetails[0].PackageIdentifier.Value = Req.TrackingId;

            request.SelectionDetails[0].PackageIdentifier.Type = TrackIdentifierType.TRACKING_NUMBER_OR_DOORTAG;
            //
            // Date range is optional.
            // If omitted, set to false
            request.SelectionDetails[0].ShipDateRangeBegin = DateTime.Parse("06/18/2000"); //MM/DD/YYYY
            request.SelectionDetails[0].ShipDateRangeEnd = request.SelectionDetails[0].ShipDateRangeBegin.AddDays(0);
            request.SelectionDetails[0].ShipDateRangeBeginSpecified = false;
            request.SelectionDetails[0].ShipDateRangeEndSpecified = false;
            //
            // Include detailed scans is optional.
            // If omitted, set to false
            request.ProcessingOptions = new TrackRequestProcessingOptionType[1];
            request.ProcessingOptions[0] = TrackRequestProcessingOptionType.INCLUDE_DETAILED_SCANS;
            return request;
        }

        public override TrackingResponse.STATUS_TYPE GetStatus()
        {
            if(metaResponse.CompletedTrackDetails != null
                && metaResponse.CompletedTrackDetails.Length > 0
                && metaResponse.CompletedTrackDetails[0].TrackDetails != null
                && metaResponse.CompletedTrackDetails[0].TrackDetails.Length > 0
                && metaResponse.CompletedTrackDetails[0].TrackDetails[0].StatusDetail != null
                && metaResponse.CompletedTrackDetails[0].TrackDetails[0].StatusDetail.Code != null)
            {
                switch(metaResponse.CompletedTrackDetails[0].TrackDetails[0].StatusDetail.Code)
                {
                    case "AA":
                    case "AD":
                    case "AF":
                    case "EO":
                    case "FD":
                    case "IT":
                    case "LO":
                    case "DP":
                    case "DS":
                    case "PL":
                    case "PF":
                    case "PU":
                    case "SF":
                    case "AR":
                    case "CD":
                    case "CC":
                    case "AX":
                    case "OF":
                    case "RR":
                    case "OX":
                    case "CP":
                        return TrackingResponse.STATUS_TYPE.EN_ROUTE;
                    case "DE":
                    case "CA":
                    case "CH":
                    case "DY":
                    case "SE":
                        return TrackingResponse.STATUS_TYPE.DELAYED;
                    case "HL":
                    case "DL":
                        return TrackingResponse.STATUS_TYPE.DELIVERED;
                    case "ED":
                    case "OD":
                        return TrackingResponse.STATUS_TYPE.OUT_FOR_DELIVERY;
                    case "AP":
                    case "EP":
                    case "OC":
                        return TrackingResponse.STATUS_TYPE.SHIPPING;
                }
            }
            
            return TrackingResponse.STATUS_TYPE.UNKNOWN;
        }
        public override DateTime? GetEta()
        {
            if(metaResponse.CompletedTrackDetails != null
                && metaResponse.CompletedTrackDetails.Length > 0
                && metaResponse.CompletedTrackDetails[0].TrackDetails != null
                && metaResponse.CompletedTrackDetails[0].TrackDetails.Length > 0)
            {
                return metaResponse.CompletedTrackDetails[0].TrackDetails[0].EstimatedDeliveryTimestamp;
            }
            return null;
        }
        public override string GetWeight()
        {
            return "";
        }
        public override string GetPresentTimeStamp()
        {
            return "";
        }
        public override string GetPresentActivity()
        {
            return "";
        }
        public override TrackingResponse.LocationInfo GetDestination()
        {
            if(metaResponse.CompletedTrackDetails != null
                && metaResponse.CompletedTrackDetails.Length > 0
                && metaResponse.CompletedTrackDetails[0].TrackDetails != null
                && metaResponse.CompletedTrackDetails[0].TrackDetails.Length > 0
                && metaResponse.CompletedTrackDetails[0].TrackDetails[0].DestinationAddress != null)
            {
                TrackingResponse.LocationInfo locationInfo = new TrackingResponse.LocationInfo()
                {
                    City = metaResponse.CompletedTrackDetails[0].TrackDetails[0].DestinationAddress.City,
                    State = metaResponse.CompletedTrackDetails[0].TrackDetails[0].DestinationAddress.StateOrProvinceCode,
                    PostalCode = metaResponse.CompletedTrackDetails[0].TrackDetails[0].DestinationAddress.PostalCode
                };
                return locationInfo;
            }
            
            return null;
        }
        public override string GetService()
        {
            return "";
        }

    }
}
