﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;
using System.Text.RegularExpressions;

namespace TrackShipping
{
    public static class Utils
    {
        /// <summary>
        /// Converts generic postal code to 4444-55555 format
        /// </summary>
        /// <param name="rawCode">postal code in raw format</param>
        /// <returns>Formated postal code</returns>
        public static string PresentPostalCode(string rawCode)
        {
            if (!string.IsNullOrEmpty(rawCode))
            {
                rawCode = rawCode.Trim();
                if (Regex.IsMatch(rawCode, @"^\d{9}$"))
                {
                    return rawCode.Substring(0, 4) + rawCode.Substring(4, rawCode.Length);
                }
            }

            return rawCode;
        }

        /// <summary>
        /// Converts generic location details to properly cased format
        /// </summary>
        /// <param name="locationString">Location string</param>
        /// <returns>Formated location string</returns>
        public static string PresentLocationString(string locationString)
        {
            var locations = locationString.Split(',');
            List<string> convertedLocations = new List<string>();
            foreach (var location in locations)
            {
                string loc = location.Trim();
                if (loc.Length > 2)
                {
                    convertedLocations.Add(ToTitleCase(loc));
                }
            }

            string finalLocationString = string.Empty;
            foreach (var location in convertedLocations)
            {
                finalLocationString += location + ", ";
            }

            return finalLocationString.Substring(0, finalLocationString.Length - 1);
        }

        /// <summary>
        /// Converts generic location details to properly cased format
        /// </summary>
        /// <param name="city">unformated city</param>
        /// <param name="stateCode">unformated stateCode</param>
        /// <param name="countryCode">unformated countryCode</param>
        /// <param name="postalCode">unformated postal ccode</param>
        /// <returns>Formated location string</returns>
        public static string PresentLocation(string city, string stateCode, string countryCode, string postalCode)
        {
            string address = string.Empty;
            if (!string.IsNullOrEmpty(city))
            {
                city = ToTitleCase(city);
            }

            if (!string.IsNullOrEmpty(stateCode))
            {
                stateCode = stateCode.Trim();
                if (stateCode.Length > 3)
                {
                    stateCode = ToTitleCase(stateCode);
                    if (!string.IsNullOrEmpty(city))
                    {
                        address = city + ", " + stateCode;
                    }
                    else
                    {
                        address = stateCode;
                    }
                }
            }
            else
            {
                address = city;
            }

            postalCode = PresentPostalCode(postalCode);

            if (!string.IsNullOrEmpty(countryCode))
            {
                countryCode = countryCode.Trim();
                if (countryCode.Length > 3)
                {
                    countryCode = ToTitleCase(countryCode);
                    if (!string.IsNullOrEmpty(address))
                    {
                        if (countryCode != "US")
                        {
                            address = address + ", " + countryCode;
                        }
                    }
                    else
                    {
                        address = countryCode;
                    }
                }

                if (!string.IsNullOrEmpty(postalCode))
                {
                    address = address + ", " + postalCode;
                }
                else
                {
                    address = postalCode;
                }
            }

            return address;
        }

        /// <summary>
        /// Converts unformatted string to title cased string
        /// </summary>
        /// <param name="source">source string</param>
        /// <returns>titled case string</returns>
        public static string ToTitleCase(string source)
        {
            TextInfo textInfo = new CultureInfo("en-US", false).TextInfo;
            return textInfo.ToTitleCase(source);
        }

        #region TextConversions
        /// <summary>
        /// To convert a Byte Array of Unicode values (UTF-8 encoded) to a complete String.
        /// </summary>
        /// <param name="characters">Unicode Byte Array to be converted to String</param>
        /// <returns>String converted from Unicode Byte Array</returns>
        private static String UTF8ByteArrayToString(Byte[] characters)
        {
            UTF8Encoding encoding = new UTF8Encoding();
            String constructedString = encoding.GetString(characters);
            return (constructedString);
        }

        /// <summary>
        /// Converts the String to UTF8 Byte array and is used in De serialization
        /// </summary>
        /// <param name="pXmlString"></param>
        /// <returns></returns>
        private static Byte[] StringToUTF8ByteArray(String pXmlString)
        {
            UTF8Encoding encoding = new UTF8Encoding();
            Byte[] byteArray = encoding.GetBytes(pXmlString);
            return byteArray;
        }
        #endregion

        public static string Serialize(object obj)
        {
            string result = JsonConvert.SerializeObject(obj, Formatting.Indented, new JsonSerializerSettings { ReferenceLoopHandling = ReferenceLoopHandling.Ignore, PreserveReferencesHandling = PreserveReferencesHandling.None });
            return result;
        }

        public static object Deserialize(string json, Type type)
        {
            return JsonConvert.DeserializeObject(json, type);
        }
    }
}
