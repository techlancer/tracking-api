﻿namespace TrackShipping
{
    public class TrackingRequest
    {
        string carrierName, trackingId;

        public string TrackingId
        {
            get { return trackingId; }
            set { trackingId = value; }
        }

        public string CarrierName
        {
            get { return carrierName; }
            set { carrierName = value; }
        }
        public string ServiceUrl { set; get; }
        public TrackingRequest(string CarrierName, string TrackingId)
        {
            carrierName = CarrierName;
            trackingId = TrackingId;
        }
        public TrackingRequest() { }
    }
}
