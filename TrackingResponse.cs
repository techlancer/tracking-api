﻿using System;
using System.Collections.Generic;

namespace TrackShipping
{
    public class TrackingResponse
    {
        public enum STATUS_TYPE
        {
            UNKNOWN = 0,
            SHIPPING = 1,
            EN_ROUTE = 2,
            OUT_FOR_DELIVERY = 3,
            DELIVERED = 4,
            DELAYED = 5,
        };
        public TrackingResponse()
        {
            Activities = new List<ActivityInfo>();
        }

        private string _TrackingNumber;
        /// <summary>
        /// The tracking number for the package
        /// </summary>
        public string TrackingNumber
        {
            get { return _TrackingNumber; }
            set { _TrackingNumber = value; }
        }

        private string _Summary;
        /// <summary>
        /// Summary information for the package
        /// </summary>
        public string Summary
        {
            get { return _Summary; }
            set { _Summary = value; }
        }
        
        private LocationInfo _Destination;
        /// <summary>
        /// Tracking Details
        /// </summary>
        public LocationInfo Destination
        {
            get { return _Destination; }
            set { _Destination = value; }
        }

        private List<ActivityInfo> _activities;
        /// <summary>
        /// Tracking Activities
        /// </summary>
        public List<ActivityInfo> Activities
        {
            get { return _activities; }
            set { _activities = value; }
        }
        
        private DateTime? _eta;
        /// <summary>
        /// Expected Time of arrival
        /// </summary>
        public DateTime? ETA
        {
            get {
                if (_eta.HasValue && _eta.Value.Year == 0001)
                    return null;
                return _eta; }
            set { _eta = value; }
        }
        
        private string _API_Response_Message;
        /// <summary>
        /// Service Details
        /// </summary>
        public string API_Response_Message
        {
            get { return _API_Response_Message; }
            set { _API_Response_Message = value; }
        }

        private STATUS_TYPE _status;
        /// <summary>
        /// Tracking Status of Type STATUS_TYPE
        /// </summary>
        public STATUS_TYPE Status
        {
            get { return _status; }
            set { _status = value; }
        }
        private bool apiResponse { get; set; }
        public bool API_Response
        {
            get { return apiResponse; }
            set { apiResponse = value; }
        }

        public class ActivityInfo
        {
            private string _city;
            /// <summary>
            /// Activity city
            /// </summary>
            public string City
            {
                get { return _city; }
                set { _city = value; }
            }

            private string _stateCode;
            /// <summary>
            /// Activity state code. e.g. LA, WA etc...
            /// </summary>
            public string StateCode
            {
                get { return _stateCode; }
                set { _stateCode = value; }
            }

            private string _postalCode;
            /// <summary>
            /// Postal code for activity. Will be later convered to 4444-55555 format
            /// </summary>
            public string PostalCode
            {
                get { return _postalCode; }
                set { _postalCode = value; }
            }

            private string _countryCode;
            /// <summary>
            /// Activity Country code
            /// </summary>
            public string CountryCode
            {
                get { return _countryCode; }
                set { _countryCode = value; }
            }

            private string _timeStamp;
            /// <summary>
            /// Activity time stamp
            /// </summary>
            public string TimeStamp
            {
                get { return _timeStamp; }
                set { _timeStamp = value; }
            }

            private string _details;
            /// <summary>
            /// Activity detailed description
            /// </summary>
            public string Details
            {
                get { return _details; }
                set { _details = value; }
            }
        }
        /// <summary>
        /// Class modelling location details
        /// </summary>
        public class LocationInfo
        {
            private string _city;
            /// <summary>
            /// Location city
            /// </summary>
            public string City
            {
                get { return _city; }
                set { _city = value; }
            }

            private string _state;
            /// <summary>
            /// Location State
            /// </summary>
            public string State
            {
                get { return _state; }
                set { _state = value; }
            }

            private string _zip;
            /// <summary>
            /// Location postal code
            /// </summary>
            public string PostalCode
            {
                get { return _zip; }
                set { _zip = value; }
            }

            /// <summary>
            /// Overriden toString method to format location details as per requirement
            /// </summary>
            /// <returns></returns>
            public override string ToString()
            {
                return Utils.PresentLocation(_city, _state, string.Empty, _zip);
            }
        }
    }
}
