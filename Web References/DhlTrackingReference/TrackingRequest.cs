﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TrackShipping.Web_References.DhlTrackingReference
{
    public class ECommerce
    {
        public Requestor Requestor { get; set; }

        public Track Track { get; set; }
    }

    public class Requestor
    {
        public string ID { get; set; }
        public string Password { get; set; }
    }

    public class Track
    {
        public Shipment Shipment { get; set; }
    }

    public class Shipment
    {
        public string TrackingNumber { get; set; }
        public Result Result { get; set; }

        public string Weight { get; set; }

        public TrackingResponse.LocationInfo Location { get; set; }
       
    }

    public class TrackReply : TrackingResponse
    {
        public ECommerce ECommerce { get; set; }   
    
    }

    public class Result
    {
        public string Code { get; set; }

        public string Description { get; set; }
    }

    public class TrackService
    {
        public TrackService(string serviceURL)
        {
            // To be Replaced with actual proxy implementation
        }
        public TrackReply Track(ECommerce request)
        {
            // Mocking actual service impementation
            TrackReply reply = new TrackReply();
            return reply;
        }
    }
}
