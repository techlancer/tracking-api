﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TrackShipping.Web_References.A1TrackingReference
{
    public class TrackReply : TrackingResponse
    {
        public AmazonTrackingResponse AmazonTrackingResponse { get; set; }
    }

    public class AmazonTrackingResponse
    {
        public PackageTrackingInfo PackageTrackingInfo { get; set; }

        public TrackingErrorInfo TrackingErrorInfo { get; set; }

        public List<TrackingEventHistory> TrackingEventHistory { get; set; }
    }

    public class PackageTrackingInfo
    {
        public string TrackingNumber { get; set; }
    }

    public class TrackingErrorInfo
    {
        public List<TrackingErrorDetail> TrackingErrorDetail { get; set; }
    }

    public class TrackingErrorDetail
    {
        public string ErrorDetailCodeDesc { get; set; }
    }

    public class TrackingEventHistory
    {
        public List<TrackingEventDetail> TrackingEventDetail { get; set; }
    }

    public class TrackingEventDetail
    {
        public string EventLocation { get; set; }
        public string EventCode { get; set; }
        public DateTime EventDateTime { get; set; }
        public string  EventCodeDesc { get; set; }
        public DateTime EstimatedDeliveryDate { get; set; }
        public TrackShipping.TrackingResponse.LocationInfo PackageDestinationLocation { get; set; }
    }
    
    public class TrackService
    {
        public TrackService(string serviceURL)
        {
            // To be Replaced with actual proxy implementation
        }
        public TrackReply Track(TrackingRequest request)
        {
            // Mocking actual service impementation
            TrackReply reply = new TrackReply();
            return reply;
        }
    }
}
