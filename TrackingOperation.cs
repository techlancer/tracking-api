﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TrackShipping
{
    public abstract class TrackingOperation<TRequest> where TRequest : TrackingRequest
    {
        public abstract TrackingResponse Track(TRequest request);
        public abstract TrackingResponse.STATUS_TYPE GetStatus();
        public abstract DateTime? GetEta();
        public abstract string GetWeight();
        public abstract string GetPresentTimeStamp();
        public abstract string GetPresentActivity();
        public abstract TrackingResponse.LocationInfo GetDestination();
        public abstract string GetService();
    }
}
